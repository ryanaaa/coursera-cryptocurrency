import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TxHandler {
	private UTXOPool utxoPool;

    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public TxHandler(UTXOPool utxoPool) {
    	this.utxoPool = new UTXOPool(utxoPool);
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, 
     * (2) the signatures on each input of {@code tx} are valid, 
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
    	Set<UTXO> inputPrevUTXOSet = new HashSet<>();
    	double valueInput = 0, valueOutput = 0;
    	for (int inputIndex = 0; inputIndex < tx.numInputs(); inputIndex++) {
    		Transaction.Input input = tx.getInput(inputIndex);
    		UTXO inputPrevUTXO = new UTXO(input.prevTxHash, input.outputIndex);
    		// (1) check output in pool
    		if (!this.utxoPool.contains(inputPrevUTXO)) {
    			return false;
    		}
    		Transaction.Output inputPrevOut = this.utxoPool.getTxOutput(inputPrevUTXO); 
    		byte[] message = tx.getRawDataToSign(inputIndex);
    		// (2) verify signature
    		if (!Crypto.verifySignature(inputPrevOut.address, message, input.signature)) {
    			return false;
    		}
    		// (3) no UTXO is claimed multiple times
    		if (inputPrevUTXOSet.contains(inputPrevUTXO)) {
    			return false;
    		}
    		inputPrevUTXOSet.add(inputPrevUTXO);
    		valueInput += inputPrevOut.value;
    	}
    	for (int outputIndex = 0; outputIndex < tx.numOutputs(); outputIndex++) {
    		Transaction.Output output = tx.getOutput(outputIndex);
    		// (4) value >= 0
    		if (output.value < 0) {
    			return false;
    		}
    		valueOutput += output.value;
    	}
    	// (5) input >= output
    	if (valueInput < valueOutput)
    		return false;
    	return true;
    }
    
    private void insertTransaction(Transaction tx) {
    	for (int inputIndex = 0; inputIndex < tx.numInputs(); inputIndex++) {
    		Transaction.Input input = tx.getInput(inputIndex);
    		UTXO inputPrevUTXO = new UTXO(input.prevTxHash, input.outputIndex);
    		this.utxoPool.removeUTXO(inputPrevUTXO);
    	}
    	for (int outputIndex = 0; outputIndex < tx.numOutputs(); outputIndex++) {
    		Transaction.Output output = tx.getOutput(outputIndex);
    		UTXO newUTXO = new UTXO(tx.getHash(), outputIndex);
    		this.utxoPool.addUTXO(newUTXO, output);
    	}
    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
    	boolean[] isValid = new boolean[possibleTxs.length];
    	int isValidCount = 0;
    	// Note that a transaction can reference another in the same block.
    	// but it seems also pass the test in coursera without caring this constraint
    	while (true) {
    		boolean hasMoreValid = false;
    		for (int ti = 0; ti < possibleTxs.length; ti++) {
    			if (isValid[ti])
    				continue;
    			Transaction tx = possibleTxs[ti];
    			if (this.isValidTx(tx)) {
	    			isValid[ti] = true;
	    			isValidCount++;
	    			this.insertTransaction(tx);
	    			hasMoreValid = true;
    			}
    		}
	    	if (!hasMoreValid)
	    		break;
    	}
    	Transaction[] resultArr = new Transaction[isValidCount];
    	for (int ti = 0, ri = 0; ti < possibleTxs.length; ti++) {
			if (isValid[ti])
				resultArr[ri++] = possibleTxs[ti];
    	}
    	return resultArr;
    }

}
